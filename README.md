# Koboldworks – Pause Control

![Supported Foundry Versions: 11-12](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fversion%3Fstyle%3Dflat%26url%3Dhttps%3A%2F%2Fgitlab.com%2Fkoboldworks%2Fagnostic%2Fpause-control%2F-%2Freleases%2Fpermalink%2Flatest%2Fdownloads%2Fmodule.json)
![Supported Game Systems: All](https://img.shields.io/endpoint?url=https%3A%2F%2Ffoundryshields.com%2Fsystem%3FnameType%3Dfull%26showVersion%3D1%26style%3Dflat%26url%3Dhttps%3A%2F%2Fimg.shields.io%2Fendpoint%3Furl%3Dhttps%253A%252F%252Ffoundryshields.com%252Fversion%253Fstyle%253Dflat%2526url%253Dhttps%253A%252F%252Fgitlab.com%252Fkoboldworks%252Fagnostic%252Fpause-control%252F-%252Freleases%252Fpermalink%252Flatest%252Fdownloads%252Fmodule.json)

Offers several options for controlling pause state.

- Unpause on Foundry VTT ready state
- Unpause on combat start
- Restore pause state as it was before combat
- Paused combat where pause state is toggled depending if it's player turn or not.

## Install

Manifest URL: <https://gitlab.com/koboldworks/agnostic/pause-control/-/releases/permalink/latest/downloads/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
