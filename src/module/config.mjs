export const CFG = /** @type {const} */ ({
	id: 'koboldworks-pause-control',
	COLORS: {
		main: 'color:orangered',
		label: 'color:mediumseagreen',
		unset: 'color:unset',
		number: 'color:mediumpurple',
	},
});
