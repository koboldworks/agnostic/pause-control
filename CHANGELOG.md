# Change Log

## 1.2.0

- Refactor: Support removed for Foundry v10 and older.

## 1.1.0.3

- Fix: Unpause on ready did not work with v10 anymore [#1]

## 1.1.0.2

- Fix: Lingering v10 compatibility issue

## 1.1.0.1

- Fix: Release building

## 1.1.0

- Foundry v10 compatibility confirmation
- New release mechanism for smaller download & install sizes.

## 1.0.1

- Maintenance update.
- Bundling swapped from rollup to esbuild.

## 1.0.0.2

- Fixed: Settings reset did not work.

## 1.0.0 Dedicated Module

- Fixed: Restore pause state now remembers pre-combat state even if GM refreshes or server is restarted.
